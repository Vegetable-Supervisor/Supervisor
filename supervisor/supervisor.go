package supervisor

import "sync"

// A Supervisor is logical aggregate of multiple remote greenhouses
type Supervisor struct {
	greenhouses        map[uint64]Greenhouse        // connected greenhouses indexed by ID
	pendingGreenhouses map[uint64]PendingGreenhouse // unapproved greenhouses indexed by ID
	mutex              *sync.Mutex                  // for concurrent access of the greenhouses
	nextID             uint64                       // next id for greenhouse indexation
}

// New constructs a new empty Supervisor
func New() *Supervisor {
	return &Supervisor{
		greenhouses:        make(map[uint64]Greenhouse),
		pendingGreenhouses: make(map[uint64]PendingGreenhouse),
		mutex:              &sync.Mutex{},
		nextID:             0,
	}
}

// Greenhouses return a copy of the attached greenhouses of the supervisor
func (sv *Supervisor) Greenhouses() *map[uint64]Greenhouse {
	m := make(map[uint64]Greenhouse)

	for k, v := range sv.greenhouses {
		m[k] = v
	}
	return &m
}
