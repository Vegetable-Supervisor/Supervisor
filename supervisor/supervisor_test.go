package supervisor

import "testing"

func mapEquals(m1, m2 map[uint64]Greenhouse) bool {
	// check that every key of m1 is in m2
	for k := range m1 {
		if _, ok := m2[k]; !ok {
			return false
		}
	}

	// check that every key of m2 is in m1
	for k2, v2 := range m2 {
		if v1, ok := m2[k2]; !ok || v1 != v2 {
			return false
		}
	}
	return true
}

// TestGreenhouses tests the Greenhouses method of a Supervisor
func TestGreenhouses(t *testing.T) {

	sv := New()

	tt := []struct {
		name string
		ghs  map[uint64]Greenhouse
	}{
		{"no greenhouse", nil},
		{
			"one greenhouse",
			map[uint64]Greenhouse{1: Greenhouse{ID: 1}},
		},
		{
			"two greenhouses",
			map[uint64]Greenhouse{1: Greenhouse{ID: 1}, 2: Greenhouse{ID: 2}},
		},
	}

	for _, tc := range tt {
		sv.greenhouses = tc.ghs
		ghs := sv.Greenhouses()

		if !mapEquals(*ghs, sv.greenhouses) {
			t.Fatalf("%s: different maps, expected: %v, got: %v", tc.name, tc.ghs, ghs)
		}

	}

}
