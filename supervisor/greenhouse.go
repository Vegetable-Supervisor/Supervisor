package supervisor

// A Greenhouse is a greenhouse connected to a Supervisor
type Greenhouse struct {
	Name string
	IP   string
	Port uint64
	ID   uint64
}

// A PendingGreenhouse is a Greenhouse that is not yet connected to a Supervisor
type PendingGreenhouse struct {
	Greenhouse
}
