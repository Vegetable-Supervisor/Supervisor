package webserver

import (
	"testing"
)

var templateNames = []string{"index.html", "information.html", "pending.html"}

// sliceSetEqual returns True if and only if every string in sl1 is in sl2 and every string in sl2 is in sl1
func sliceSetEqual(sl1, sl2 []string) bool {
	for _, el1 := range sl1 {
		match := false
		for _, el2 := range sl2 {
			if el1 == el2 {
				// match
				match = true
				break
			}
		}
		if !match {
			// no match for el1
			return false
		}
	}
	// here sl1 is contained in sl2 as set
	return len(sl1) == len(sl2)
}

// TestLoadTemplates tests the LoadTemplates function
func TestLoadTemplates(t *testing.T) {

	tt := []struct {
		name  string
		path  string
		names []string
	}{
		{"path 'templates'", "templates", templateNames},
		{"path './templates'", "./templates", templateNames},
		{"path 'templates/'", "./templates", templateNames},
	}

	for _, tc := range tt {
		templ := loadTemplates(tc.path)
		templates := templ.Templates()
		var names []string
		// test that all and only expected templates have been parsed
		for _, template := range templates {
			if template.Name() != "" {
				names = append(names, template.Name())
			}
		}

		if !sliceSetEqual(names, templateNames) {
			t.Fatalf("%s: did not parse correct files, expected: %v, got: %v", tc.name, templateNames, names)
		}
	}
}
