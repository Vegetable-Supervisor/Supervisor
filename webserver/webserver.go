package webserver

import (
	"html/template"
	"log"
	"net/http"

	"gitlab.com/Vegetable-Supervisor/Supervisor/supervisor"
)

// A WebServer is a controller for a Supervisor.
// It provides the view to users as webpages.
type WebServer struct {
	http.Server
	supervisor *supervisor.Supervisor
	templates  *template.Template
	mux        *http.ServeMux
}

// New constructs and returns a new WebServer from a Supervisor, a path to html templates,
// and a http.Server that provides http configuration.
// the http.Server's handler will be overriden with the handler for the supervisor.
// The WebServer needs to be launched after its creation.
// If 'templatePath' is the zero string, default path of './templates' is used
func New(sv *supervisor.Supervisor, server http.Server, templatePath string) *WebServer {
	if templatePath == "" {
		templatePath = "./templates"
	}
	templates := loadTemplates(templatePath)

	mux := http.NewServeMux()

	serv := &WebServer{
		Server:     server,
		supervisor: sv,
		templates:  templates,
		mux:        mux,
	}

	// routes
	mux.HandleFunc("/", serv.homeHandler)
	mux.HandleFunc("/pending", serv.pendingHandler)
	mux.HandleFunc("/greenhouse", serv.greenhouseHandler)

	serv.Server.Handler = mux

	return serv
}

func (serv *WebServer) homeHandler(w http.ResponseWriter, r *http.Request) {
	// home view
	// approved greenhouses are listed
	if r.URL.Path != "/" {
		w.WriteHeader(http.StatusNotFound)
	}

	err := serv.templates.ExecuteTemplate(w, "index.html", serv.supervisor.Greenhouses())
	if err != nil {
		log.Printf("could not execute template: %v", err)
	}
}

func (serv *WebServer) pendingHandler(w http.ResponseWriter, r *http.Request) {
	// pending view
	// unapproved greenhouses are listed
	if r.URL.Path != "/pending" {
		w.WriteHeader(http.StatusNotFound)
	}

	err := serv.templates.ExecuteTemplate(w, "pending.html", serv.supervisor.Greenhouses())
	if err != nil {
		log.Printf("could not execute template: %v", err)
	}
}

func (*WebServer) greenhouseHandler(w http.ResponseWriter, r *http.Request) {
	// greenhouse view
	// specific greenhouse view
	w.WriteHeader(http.StatusOK)
}
