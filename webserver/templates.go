package webserver

import (
	"fmt"
	"html/template"
	"os"
	"path/filepath"
	"strings"
)

// loadTemplates loads html templates from a directory located at 'root' path
// returns a *template.Template combining all of the parsed templates and a base template ""
func loadTemplates(root string) *template.Template {
	templ := template.New("")
	err := filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
		if strings.HasSuffix(path, ".html") {
			_, err = templ.ParseFiles(path) // parse template at path and add it to templ
			if err != nil {
				return fmt.Errorf("could not parse template named %s: %v", path, err)
			}
		}
		return nil
	})

	if err != nil {
		// a template could not be parsed
		panic(err)
	}

	return templ
}
