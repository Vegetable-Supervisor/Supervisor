package webserver

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/Vegetable-Supervisor/Supervisor/supervisor"
)

// TestServeMux tests the ServeMux of a Supervisor,
// that is, the routing of HTTP request to the correct handlers
func TestServeMux(t *testing.T) {

	sv := supervisor.New()
	serv := New(sv, http.Server{}, "")

	tt := []struct {
		name string
		path string

		// expected
		pattern string
		status  int
	}{
		{name: "home page", path: "/", pattern: "/", status: http.StatusOK},
		{name: "pending page", path: "/pending", pattern: "/pending", status: http.StatusOK},
		{name: "greehouse page", path: "/greenhouse", pattern: "/greenhouse", status: http.StatusOK},
		{name: "nonexistent page 'error'", path: "/error", pattern: "/", status: http.StatusNotFound},
	}

	for _, tc := range tt {
		req, err := http.NewRequest("GET", "http://localhost:8080"+tc.path, nil)
		if err != nil {
			t.Fatalf("could not create http request: %v", err)
		}
		// get the handler and pattern matching the request from the ServeMux of the WebServer
		handler, pattern := serv.mux.Handler(req)

		if pattern != tc.pattern {
			t.Fatalf("%s: wrong matched pattern for path '%s', expected: '%s', got: '%s'", tc.name, tc.path, tc.pattern, pattern)
		}

		// mock of a ResponseWriter
		rec := httptest.NewRecorder()

		// handle the request
		handler.ServeHTTP(rec, req)

		res := rec.Result()
		defer res.Body.Close()

		if res.StatusCode != tc.status {
			t.Fatalf("%s: wrong status code for path '%s', expected: '%d', got: '%d'", tc.name, tc.path, tc.status, res.StatusCode)
		}

	}
}
