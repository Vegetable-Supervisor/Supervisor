PROJECT_NAME := "Supervisor"
PKG := "gitlab.com/Vegetable-Supervisor/$(PROJECT_NAME)"
PKG_LIST := $(shell go list ${PKG}/...)

.PHONY: all dep build clean test coverage coverhtml lint

all: build

lint: ## lint files
	@golint -set_exit_status ./...

test: ## run unit tests
	@go test -short ./...

race: ## test ata race detector
	@go test -race -short ./...

coverage: ## get coverage
	@go test ./... -cover -v

dependency: ## install dependencies
	@go get -u github.com/golang/lint/golint
	@go get -v -d ./...

build: dependency ## build
	@go build -i -v $(PKG)

clean: ## delete binary
	@rm -f $(PROJECT_NAME)

help: ## print this help
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
