package servicereg

import (
	"testing"

	ssdp "github.com/koron/go-ssdp"
)

// Test a ServiceRegistry when two different Services are registered and queried
func TestServiceRegistryRetrieval(t *testing.T) {
	// create a ServiceRegistry
	sr := New("my_service", 1, "127.0.0.1:4321")
	// create a first Service
	sv1 := Service{
		serviceType: "my_service",
		usn:         "sv1",
	}
	sv2 := Service{
		serviceType: "my_service",
		usn:         "sv2",
	}

	// register
	sr.register(sv1)
	sr.register(sv2)

	// test retrieval
	sr.Mutex.Lock()
	defer sr.Mutex.Unlock()
	if _, ok := sr.Registry["sv1"]; !ok {
		t.Fatalf("ServiceRegistry %v should have registered Service %v", sr, sv1)
	}
	if _, ok := sr.Registry["sv2"]; !ok {
		t.Fatalf("ServiceRegistry %v should have registered Service %v", sr, sv1)
	}
}

// Test a ServiceRegistry when a Service with wrong service type is registered
// The ServiceRegistry should not store it
func TestServiceRegistryWrongServiceType(t *testing.T) {
	// create a ServiceRegistry
	sr := New("my_service", 1, "127.0.0.1:4321")
	// create a Service with different service type
	sv := Service{
		serviceType: "a_different_service",
		usn:         "different_service_usn",
	}

	sr.register(sv)
	sr.Mutex.Lock()
	defer sr.Mutex.Unlock()
	if _, ok := sr.Registry["different_service_usn"]; ok {
		t.Fatalf("ServiceRegistry should not register services with different service type")
	}
}

// Test a ServiceRegistry when a Service is discovered two times
// the ServiceRegistry should update the 'last_on' property of the Service
func TestServiceRegistryUpdateTime(t *testing.T) {
	// create a ServiceRegistry
	sr := New("my_service", 1, "127.0.0.1:4321")

	// create a ssdp.Service
	ssdpService := ssdp.Service{
		Type:     "my_service",
		USN:      "service_usn",
		Location: "http://my.service.test",
	}
	// construct a Service fro the ssdp.Service
	sv, err := new(ssdpService)
	if err != nil {
		t.Fatalf("convertion of ssdp.Service %v failed: %v", ssdpService, err)
	}

	// register
	sr.register(*sv)
	sr.Mutex.Lock()
	lastUp := (*sr.Registry[sv.usn]).LastOn()
	sr.Mutex.Unlock()

	// create new service later
	sv, err = new(ssdpService)
	if err != nil {
		t.Fatalf("convertion of ssdp.Service %v failed: %v", ssdpService, err)
	}

	// register again
	sr.register(*sv)
	sr.Mutex.Lock()
	newLastUp := (*sr.Registry[sv.usn]).LastOn()
	sr.Mutex.Unlock()

	// check time update
	if newLastUp.UnixNano() <= lastUp.UnixNano() {
		t.Fatalf("register a service two times did not update the 'lastOn' time of Service")
	}
}

// Test the registerServices method of a ServiceRegistry
func TestServiceRegisterServices(t *testing.T) {
	// create a ServiceRegistry
	sr := New("my_service", 1, "127.0.0.1:4321")

	// ssdp.Services to register
	sv1, sv2, sv3, sv4 := ssdp.Service{Location: "https://sv1", Type: "my_service", USN: "sv1"},
		ssdp.Service{Location: "https://sv2", Type: "my_service", USN: "sv2"},
		ssdp.Service{Location: "https://sv3", Type: "other_service", USN: "sv3"},
		ssdp.Service{Location: "././.", Type: "my_service", USN: "sv4"}

	ssdpServices := []ssdp.Service{sv1, sv2, sv3, sv4}

	sr.registerServices(ssdpServices)

	sr.Mutex.Lock()
	defer sr.Mutex.Unlock()

	if len(sr.Registry) != 2 {
		if len(sr.Registry) > 2 {
			t.Fatalf("ServiceRegistry did register wrong service type %v", sv3)
		} else {
			t.Fatalf("ServiceRegistry did not register correct services")
		}
	}
}
