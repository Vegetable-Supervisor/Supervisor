package servicereg

import (
	"fmt"
	"net/url"
	"testing"

	ssdp "github.com/koron/go-ssdp"
)

// equals returns True if and only if the ssdp.Service equals the Service,
// that is, they match their Type, USN and Location
func equals(ss ssdp.Service, s Service) bool {
	b := ss.Type == s.Type() && ss.USN == s.Identifier() && ss.Location == s.Location()
	return b
}

// TestNewService tests the new function to create a Service from a ssdp.Service
func TestNewService(t *testing.T) {
	urlTest, _ := url.Parse("http://test.service.test")

	tt := []struct {
		name            string
		ssdpService     ssdp.Service
		expectedService Service
		err             error
	}{
		{"empty", ssdp.Service{}, Service{}, fmt.Errorf("error")},
		{
			"good service",
			ssdp.Service{
				Type:     "test type",
				USN:      "test usn",
				Location: "http://test.service.test",
				Server:   "test server",
			},
			Service{
				serviceType: "test type",
				usn:         "test usn",
				url:         urlTest,
			}, nil,
		},
		{
			"bad url service",
			ssdp.Service{
				Type:     "test type",
				USN:      "test usn",
				Location: ".,,est",
				Server:   "test server",
			},
			Service{},
			fmt.Errorf("error"),
		},
	}

	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			service, err := new(tc.ssdpService)
			if tc.err != nil && err == nil {
				t.Fatalf("converting SSDP service %v into Service should have thrown an error", tc.ssdpService)
			}
			if tc.err != nil && err != nil {
				if service != nil {
					t.Fatalf("converting SSDP service %v into Service should have returned a nil *Service due to an error", tc.ssdpService)
				}
				return
			}
			if tc.err == nil && err != nil {
				t.Fatalf("converting SSDP service %v into Service has thrown an error: %v", tc.ssdpService, err)
			}

			if !equals(tc.ssdpService, *service) {
				t.Fatalf("ssdp service %v was converted into service %v, expecting %v", tc.ssdpService, *service, tc.expectedService)
			}
		})
	}
}
