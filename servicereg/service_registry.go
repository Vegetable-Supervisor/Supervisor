package servicereg

import (
	"log"
	"sync"

	ssdp "github.com/koron/go-ssdp"
)

// A ServiceRegistry is an active database of connected HTTP services
// it is a SSDP client, for discovering new services
type ServiceRegistry struct {
	st        string                    // Service Type
	waitSec   int                       // number of seconds between each discovery trials
	localAddr string                    // local address to search from
	Mutex     sync.Mutex                // to access the Registry
	Registry  map[string]*ServiceStub // map USN -> Service
}

// New creates a new ServiceRegistry, for services of given type 'st'
// It will wait 'waitSec' seconds between each discovery trials
// And runs on 'localaddr' address.
func New(st string, waitSec int, localAddr string) *ServiceRegistry {
	return &ServiceRegistry{
		st:        st,
		waitSec:   waitSec,
		localAddr: localAddr,
		Registry:  make(map[string]*ServiceStub),
	}
}

// register registers a service into the ServiceRegistry if it matches its service type
// if the service is already registered, does nothing
func (sr *ServiceRegistry) register(service ServiceStub) {
	log.Printf("start registeration: %s", service)
	sr.Mutex.Lock()
	defer sr.Mutex.Unlock()
	if sr.st != service.Type() {
		log.Printf("registration failed: bad service type: %s", service.Identifier())
		return
	}
	if _, ok := sr.Registry[service.Identifier()]; ok {
		// service is already in Registry
		// update service up time
		log.Printf("registration update: %s", service.Identifier())
		sr.Registry[service.Identifier()] = &service
	} else {
		// new service
		log.Printf("registration success: %s", service.Identifier())
		sr.Registry[service.Identifier()] = &service
	}
}

// registerServices registers ssdp.Services into the ServiceRegistry if service type matches
// if a ssdp.Service cannot be parsed, an error is logged but not thrown
func (sr *ServiceRegistry) registerServices(ssdpServices []ssdp.Service) {
	// create service
	for _, ssdpService := range ssdpServices {
		service, err := new(ssdpService)
		if err != nil || service == nil {
			log.Printf("service %s at %s could not be parsed", ssdpService.USN, ssdpService.Location)
			continue
		}
		// register service if new
		sr.register(*service)
	}
}

// Start starts the ServiceRegistry's SSDP polling for new services,
// registering new services as they are discovered
func (sr *ServiceRegistry) Start() {
	for {
		ssdpServices, err := ssdp.Search(sr.st, sr.waitSec, sr.localAddr)
		if err != nil {
			log.Printf("could not send ssdp search: %v", err)
		}
		sr.registerServices(ssdpServices)
	}
}
