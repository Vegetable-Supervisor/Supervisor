package servicereg

import (
	"fmt"
	"net/url"
	"time"

	ssdp "github.com/koron/go-ssdp"
)

// A ServiceStub is a description of a Remote Service
type ServiceStub interface {
	Identifier() string // service identifier
	Type() string       // service type
	Location() string   // service location: url
	LastOn() time.Time  // last time the service was online
}


func (s Service) String() string {
	return fmt.Sprintf("Service{id: %s, type: %s, location: %s}", s.Identifier(), s.Type(), s.Location())
}

// A Service is an implememtation of ServiceStub,
// describing a HTTP ressource that is advertised using SSDP
type Service struct {
	serviceType string    // service type
	usn         string    // unique service identifier
	url         *url.URL  // URL of the service
	lastOn      time.Time // last time the service has been verified alive
	name        string    // name of the service
}

// Type returns the type  of the Service
func (s Service) Type() string {
	return s.serviceType
}

// Identifier returns the identifier of the Service
func (s Service) Identifier() string {
	return s.usn
}

// Location returns the location (URL) of the Service
func (s Service) Location() string {
	return s.url.String()
}

// LastOn returns the time at wich the service was seen online for the last time
func (s Service) LastOn() time.Time {
	return s.lastOn
}

// new creates a Service from a ssdp.Service
func new(ssdpService ssdp.Service) (*Service, error) {
	// check if url is valid
	_, err := url.ParseRequestURI(ssdpService.Location)
	if err != nil {
		return nil, fmt.Errorf("could not parse URL from SSDP service: %v", err)
	}

	loc, err := url.Parse(ssdpService.Location)
	if err != nil {
		return nil, fmt.Errorf("could not parse URL from SSDP service: %v", err)
	}

	service := Service{
		serviceType: ssdpService.Type,
		usn:         ssdpService.USN,
		url:         loc,
		lastOn:      time.Now(),
	}

	return &service, nil
}

