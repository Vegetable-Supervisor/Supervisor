package main

import (
	"net/http"

	"gitlab.com/Vegetable-Supervisor/Supervisor/servicereg"
	"gitlab.com/Vegetable-Supervisor/Supervisor/supervisor"
	"gitlab.com/Vegetable-Supervisor/Supervisor/webserver"
)

func main() {

	server := http.Server{
		Addr: ":4000",
	}
	supervisor := supervisor.New()
	webserver := webserver.New(supervisor, server, "webserver/templates")
	svreg := servicereg.New("greenhouse", 3, "0.0.0.0:0")

	// starts webserver
	go webserver.ListenAndServe()
	// starts service registry
	svreg.Start()
}
